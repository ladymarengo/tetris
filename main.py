from itertools import chain

import pygame

import events
import shapes
import ui
import theme


class PyGameState:
    def __init__(self):
        pygame.init()
        self.window = pygame.display.set_mode((300, 600))
        pygame.display.set_caption('Tetris')


PYGAME_STATE = None


class Game:
    def __init__(self):
        global PYGAME_STATE
        if PYGAME_STATE is None:
            PYGAME_STATE = PyGameState()

        self.window = PYGAME_STATE.window
        self.window_running = True
        self.game_running = False
        self.game_in_progress = False
        self.game_over = False
        self.ui = ui.Ui(self)
        self.clock = pygame.time.Clock()
        self.falling_blocks = None
        self.fallen_blocks = pygame.sprite.Group()

        self.ms_per_advance = 200
        self.advance_counter_ms = 0

    def tick(self):
        self.process_events()
        self.update()
        self.draw()
        pygame.display.flip()
        self.clock.tick(60)

    def process_events(self):
        for event in pygame.event.get():
            self.ui.update(event)
            if self.falling_blocks is not None:
                self.falling_blocks.update_shape(event, self.fallen_blocks)

            if event.type == pygame.QUIT:
                self.window_running = False

            if event.type == pygame.KEYDOWN:
                if self.game_in_progress and event.key == pygame.K_ESCAPE:
                    self.game_running = not self.game_running

    def update(self):
        if self.falling_blocks and self.falling_blocks.fallen:
            self.fallen_blocks.add(self.falling_blocks)
            self.check_line()
            if not self.win():
                self.falling_blocks = shapes.Shape()
            else:
                if not self.game_over:
                    self.ui.game_over()
                    self.game_over = True

        if self.game_running:
            self.advance_counter_ms += self.clock.get_time()
        if self.advance_counter_ms > self.ms_per_advance:
            self.advance_block()
            self.advance_counter_ms = self.advance_counter_ms - self.ms_per_advance

    def advance_block(self):
        pygame.event.post(pygame.event.Event(
            events.BLOCK_CREEPING
        ))

    def draw(self):
        self.window.fill(theme.CURRENT['bg'])
        self.fallen_blocks.draw(self.window)
        if self.falling_blocks is not None:
            self.falling_blocks.draw(self.window)
        self.ui.draw(self.window)

    def start_game(self):
        self.game_running = True
        self.game_in_progress = True
        self.spawn_block()

    def spawn_block(self):
        self.falling_blocks = shapes.Shape()

    def check_line(self):
        for i in range(600, 0, -30):
            blocks = 0
            for block in self.fallen_blocks:
                if block.rect.bottom == i:
                    blocks += 1
            if blocks == 10:
                events.score_up(10)
                self.ms_per_advance -= 5
                for block in self.fallen_blocks:
                    if block.rect.bottom == i:
                        block.kill()
                    elif block.rect.bottom < i:
                        block.rect.bottom += 30
                self.check_line()

    def win(self):
        for block in self.fallen_blocks:
            if block.rect.top == 0:
                return True
        return False

    def unpause(self):
        self.game_running = True

    def restart(self):
        global game
        game = Game()
        game.start_game()

    def quit(self):
        self.window_running = False

    def switch_theme(self):
        if theme.CURRENT == theme.DARK:
            theme.CURRENT = theme.LIGHT
        else:
            theme.CURRENT = theme.DARK
        self.ui.redraw(self.window)
        for s in chain(self.fallen_blocks, self.falling_blocks or []):
            s.redraw()


if __name__ == '__main__':
    game = Game()
    while game.window_running:
        game.tick()
    pygame.quit()
