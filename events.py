import pygame

SCORE_UP = pygame.USEREVENT + 1
BLOCK_CREEPING = pygame.USEREVENT + 2


def score_up(score):
    pygame.event.post(pygame.event.Event(
        SCORE_UP, score=score
    ))
