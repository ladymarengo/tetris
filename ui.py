import pygame
import functools

import events
import theme


class Shade(pygame.sprite.Sprite):
    _shade_alpha = 200

    def __init__(self, window):
        super(Shade, self).__init__()
        self.image = None
        self.rect = None
        self.redraw(window)

    @classmethod
    def get_color(cls):
        return list(theme.CURRENT['bg']) + [cls._shade_alpha]

    def redraw(self, window):
        self.image = pygame.Surface(window.get_size(), pygame.SRCALPHA, 32)
        self.rect = self.image.get_rect()
        self.image.fill(self.get_color())


class Button(pygame.sprite.Sprite):
    _all_buttons = []

    def __init__(self, text, coords, activate):
        super().__init__()
        self.text = text
        self.coords = coords
        self.activate = activate
        self.clicked = False
        self._rendered_text = self.get_rendered()
        self._highlighted_text = pygame.Surface(self._rendered_text.get_size())
        self._text_with_bg = self._highlighted_text.copy()

        self.redraw()
        self.image = self._rendered_text
        self.rect = self.image.get_rect().move(*self.coords)
        self._all_buttons.append(self)

    @classmethod
    def redraw_all(cls):
        cls._shade_surface = None
        for b in cls._all_buttons:
            if b.alive():
                b.redraw()

    def get_rendered(self):
        return get_font().render(self.text, True, theme.CURRENT['font'])

    def get_rect(self):
        return pygame.Rect(
            self.coords[0],
            self.coords[1],
            self.get_rendered().get_width(),
            self.get_rendered().get_height()
        )

    def update(self, event):

        if event.type == pygame.MOUSEBUTTONDOWN:
            x, y = event.pos
            if self.get_rect().collidepoint(x, y):
                if not self.clicked:
                    self.activate()
                    self.clicked = True
        elif event.type == pygame.MOUSEMOTION:
            x, y = event.pos
            self.clicked = False
            if self.get_rect().collidepoint(x, y):
                self.image = self._highlighted_text
            else:
                self.image = self._text_with_bg

    def redraw(self):
        self._rendered_text = self.get_rendered()
        self._highlighted_text.fill(theme.CURRENT['font_highlight'])
        self._highlighted_text.blit(self._rendered_text, self._rendered_text.get_rect())

        self._text_with_bg.fill(theme.CURRENT['bg'])
        self._text_with_bg.blit(self._rendered_text, self._rendered_text.get_rect())


class Counter(pygame.sprite.Sprite):
    def __init__(self, text, coord, init=0):
        super(Counter, self).__init__()
        self.text = text
        self.value = init
        self.image = None
        self.rect = None
        self.coord = coord

        self.redraw()

    def increase(self, value):
        self.value += value
        self.redraw()

    def redraw(self):
        rendered = get_font().render(self.text.format(self.value), True,
                                     theme.CURRENT['font'])
        bg = rendered.copy()
        bg.fill(Shade.get_color())
        bg.blit(rendered, rendered.get_rect())
        self.image = bg
        self.rect = bg.get_rect().move(*self.coord)

    def set(self, value):
        self.value = value
        self.redraw()

    def move_to(self, new_coords):
        self.coord = new_coords
        self.redraw()


class Ui:
    _left = 50

    def __init__(self, game):
        self.game = game
        self.start_btn = Button("Start", (self._left, 200), game.start_game)
        self.switch_theme_btn = Button("Switch theme", (self._left, 250),
                                       game.switch_theme)
        self.exit_btn = Button("Exit", (self._left, 300), game.quit)
        self.continue_btn = Button("Continue", (self._left, 200), game.unpause)
        self.restart_btn = Button("Restart", (self._left, 150), game.restart)
        self.shade = Shade(game.window)
        self.score = Counter("Score: {}", (0, 0))
        self.game_over_label = pygame.sprite.Sprite()
        self.redraw(game.window)

        self.main_menu = pygame.sprite.Group((
            self.start_btn,
            self.switch_theme_btn,
            self.exit_btn
        ))

        self.in_game_menu = pygame.sprite.Group((
            self.shade,
            self.continue_btn,
            self.restart_btn,
            self.switch_theme_btn,
            self.exit_btn,
            self.score
        ))

        self.in_game_ui = pygame.sprite.Group((
            self.score
        ))

    def get_active_menu(self):
        if not self.game.game_running:
            if self.game.game_in_progress:
                return self.in_game_menu
            else:
                return self.main_menu
        else:
            return self.in_game_ui

    def update(self, event):
        if event.type == events.SCORE_UP:
            self.score.increase(event.score)
        self.get_active_menu().update(event)

    def draw(self, window):
        self.get_active_menu().draw(window)

    def redraw(self, window):
        Button.redraw_all()
        self.shade.redraw(window)
        self.score.redraw()
        rendered_text = get_font().render('Game over', True, (255, 0, 0))
        self.game_over_label.image = rendered_text.copy()
        self.game_over_label.image.fill(Shade.get_color())
        self.game_over_label.image.blit(rendered_text, rendered_text.get_rect())
        self.game_over_label.rect = self.game_over_label.image.get_rect().move(70, 100)

    def game_over(self):
        self.in_game_ui.add(self.game_over_label)
        self.in_game_menu.add(self.game_over_label)
        self.score.move_to((80, 350))


@functools.lru_cache(1)
def get_font():
    return pygame.font.SysFont('Corbel', 35)
