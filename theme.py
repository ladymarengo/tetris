LIGHT = {
    'bg': (255, 255, 255),
    'block_colors': [(214,129,214), (162,129,214), (228,127,127), (129,150,214), (228,207,127), (110,197,157)],
    'font': (0, 0, 0),
    'font_highlight': (100, 100, 100)
}

DARK = {
    'bg': (0, 0, 0),
    'block_colors': 'random_gen',
    'font': (255, 255, 255),
    'font_highlight': (100, 100, 100)
}

CURRENT = LIGHT
