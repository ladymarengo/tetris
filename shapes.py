import colorsys
import random

import pygame

import events
import theme

from conditions import conditions_dict


class Block(pygame.sprite.Sprite):
    def __init__(self, name, color, rect=None, left=None, bottom=None):
        super(Block, self).__init__()
        self.name = name
        self.color = color
        self.image = pygame.Surface((30, 30))
        self.redraw()
        if not (left is None and bottom is None):
            self.rect = self.image.get_rect(left=left, bottom=bottom)
        elif rect is None:
            self.rect = self.image.get_rect(left=random.randint(0, 9) * 30, bottom=-30)
        else:
            self.rect = rect

    def redraw(self):
        self.image.fill(self.color)

    def check_collide(self, shape, move):
        copy_block = Block(name=1, color=self.color, rect=pygame.Rect.copy(self.rect))
        copy_block.rect.move_ip(*move)
        return pygame.sprite.spritecollideany(copy_block, shape)

    def update(self, move):
        self.rect.move_ip(*move)


class Shape(pygame.sprite.Group):

    def __init__(self):
        super(Shape, self).__init__()
        self.size = 30
        self.speed = 30
        self.fallen = False
        self.type = random.randint(0, 6)
        self.color = generate_color()
        self.condition = random.randint(0, len(conditions_dict[self.type]) - 1)
        self.width = conditions_dict[self.type][self.condition][0]
        self.left = (5-self.width//2) * self.size
        self.bottom = -30
        for i in range(4):
            self.add(Block(name=i, color=self.color, left=self.left + conditions_dict[self.type][self.condition][1][i][0] * self.size,
                           bottom=self.bottom + conditions_dict[self.type][self.condition][1][i][1] * self.size))

    def check_collide(self, move, fallen_blocks):
        for sprite in self:
            if sprite.check_collide(fallen_blocks, move):
                return True
        return False

    def rotate(self):
        if self.condition == len(conditions_dict[self.type])-1:
            self.condition = 0
        else:
            self.condition += 1
        self.width = conditions_dict[self.type][self.condition][0]
        while self.left+(self.width*self.size) > 300:
            self.left -= self.size
        for block in self.sprites():
            block.rect.left = self.left + conditions_dict[self.type][self.condition][1][block.name][0] * self.size
            block.rect.bottom = self.bottom + conditions_dict[self.type][self.condition][1][block.name][1] * self.size

    def update_shape(self, event, fallen_blocks):
        if self.fallen:
            return
        if event.type == events.BLOCK_CREEPING:
            if self.bottom == 600 or self.check_collide((0, self.speed), fallen_blocks):
                events.score_up(1)
                self.fallen = True
            else:
                self.update((0, self.speed))
                self.bottom += self.speed
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT and self.left > 0 and not self.check_collide((-self.speed, 0), fallen_blocks):
                self.update((-self.speed, 0))
                self.left -= self.speed
            elif event.key == pygame.K_RIGHT and self.left+(self.width*30) < 300 and not self.check_collide((self.speed, 0), fallen_blocks):
                self.update((self.speed, 0))
                self.left += self.speed
            elif event.key == pygame.K_UP:
                self.rotate()
            elif event.key == pygame.K_DOWN:
                pygame.event.post(pygame.event.Event(events.BLOCK_CREEPING))


def generate_color():
    colors = theme.CURRENT['block_colors']
    if colors == 'random_gen':
        return tuple(
            int(v * 255)
            for v in colorsys.hsv_to_rgb(
                h=random.random(),
                s=random.random() * 0.2 + 0.4,
                v=0.4)
        )
    else:
        return random.choice(colors)
